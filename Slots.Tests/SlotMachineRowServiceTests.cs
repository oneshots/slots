using Slots.Models;
using Slots.Services;

namespace Slots.Tests;

public class SlotMachineRowServiceTests
{

    private static SlotMachine SlotMachine = new SlotMachine()
    {
        Name = "Test Slot Machine",
        Description = "Test Slot Machine",
        AnimationSpeed = 10,
        RowCount = 4,
        ColumnCount = 3,
        Symbols = new List<SlotMachineSymbol>()
        {
            new SlotMachineSymbol()
            {
                Name = "Apple",
                DisplayName = "A",
                Probability = 0.45,
                Coefficient = 0.4m,
                IsWildcard = false
            },
            new SlotMachineSymbol()
            {
                Name = "Banana",
                DisplayName = "B",
                Probability = 0.35,
                Coefficient = 0.6m,
                IsWildcard = false
            },
            new SlotMachineSymbol()
            {
                Name = "Pinnaple",
                DisplayName = "P",
                Probability = 0.15,
                Coefficient = 0.8m,
                IsWildcard = false
            },
            {
                new SlotMachineSymbol()
                {
                    Name = "Wildcard",
                    DisplayName = "*",
                    Probability = 0.05,
                    Coefficient = 0,
                    IsWildcard = true
                }
            }
        }
    };

    public SlotMachineRowService CreateSUT()
    {

        return new SlotMachineRowService();

    }

    [Theory]
    [InlineData("*", "P", "*", 10, 8)]
    [InlineData("A", "A", "A", 10, 12)]
    [InlineData("B", "B", "B", 10, 18)]
    [InlineData("P", "P", "P", 10, 24)]
    [InlineData("A", "B", "P", 10, 0)]
    [InlineData("*", "A", "B", 10, 0)]
    [InlineData("*", "*", "*", 10, 0)]
    public void CalculateValue_WhenCalled_EnsureCorrectValueIsReturned(string s1, string s2, string s3, decimal stake, decimal expectedValue)
    {

        var sut = CreateSUT();

        var slotMachineRowSymbols = new List<SlotMachineSymbol>() { };
        slotMachineRowSymbols.Add(SlotMachine.Symbols.First(x => x.DisplayName == s1));
        slotMachineRowSymbols.Add(SlotMachine.Symbols.First(x => x.DisplayName == s2));
        slotMachineRowSymbols.Add(SlotMachine.Symbols.First(x => x.DisplayName == s3));

        var slotMachineRow = new SlotMachineRow() { Symbols = slotMachineRowSymbols };

        var value = sut.CalculateValue(slotMachineRow, stake);

        Assert.Equal(expectedValue, value);

    }

}