using Moq;
using Slots.Exceptions;
using Slots.Interfaces;
using Slots.Models;
using Slots.Services;

namespace Slots.Tests;

public class SlotMachineServiceTests
{

    private Mock<IRNGService> RNGServiceMock = new Mock<IRNGService>();

    public SlotMachineService CreateSUT()
    {

        return new SlotMachineService(RNGServiceMock.Object);

    }

    [Fact]
    public void ValidateAndThrow_WhenRowCountIsBelowOne_ThrowConfigurationException()
    {

        var slotMachine = new SlotMachine()
        {
            Name = "Test Slot Machine",
            Description = "A test machine",
            RowCount = -5,
            ColumnCount = 4,
            AnimationSpeed = 100,
            Symbols = new List<SlotMachineSymbol>()
            {
                new SlotMachineSymbol ()
                {
                    Name = "Test",
                    DisplayName = "Test",
                    IsWildcard = true,
                    Probability = 1,
                    Coefficient = 0
                }
            }
        };

        var sut = CreateSUT();

        var act = () => sut.ValidateAndThrow(slotMachine);

        Assert.Throws<ConfigurationException>(act);

    }

    [Fact]
    public void ValidateAndThrow_WhenColumnCountIsBelowOne_ThrowConfigurationException()
    {

        var slotMachine = new SlotMachine()
        {
            Name = "Test Slot Machine",
            Description = "A test machine",
            RowCount = 5,
            ColumnCount = -4,
            AnimationSpeed = 100,
            Symbols = new List<SlotMachineSymbol>()
            {
                new SlotMachineSymbol ()
                {
                    Name = "Test",
                    DisplayName = "Test",
                    IsWildcard = true,
                    Probability = 1,
                    Coefficient = 0
                }
            }
        };

        var sut = CreateSUT();

        var act = () => sut.ValidateAndThrow(slotMachine);

        Assert.Throws<ConfigurationException>(act);

    }

    [Fact]
    public void ValidateAndThrow_WhenAnimationSpeedIsBelowZero_ThrowConfigurationException()
    {

        var slotMachine = new SlotMachine()
        {
            Name = "Test Slot Machine",
            Description = "A test machine",
            RowCount = 5,
            ColumnCount = 4,
            AnimationSpeed = -100,
            Symbols = new List<SlotMachineSymbol>()
            {
                new SlotMachineSymbol ()
                {
                    Name = "Test",
                    DisplayName = "Test",
                    IsWildcard = true,
                    Probability = 1,
                    Coefficient = 0
                }
            }
        };

        var sut = CreateSUT();

        var act = () => sut.ValidateAndThrow(slotMachine);

        Assert.Throws<ConfigurationException>(act);

    }

    [Fact]
    public void ValidateAndThrow_WhenSymbolProbabilitiesDoNotAddToOne_ThrowConfigurationException()
    {

        var slotMachine = new SlotMachine()
        {
            Name = "Test Slot Machine",
            Description = "A test machine",
            RowCount = 5,
            ColumnCount = 4,
            AnimationSpeed = 100,
            Symbols = new List<SlotMachineSymbol>()
            {
                new SlotMachineSymbol ()
                {
                    Name = "Test",
                    DisplayName = "Test",
                    IsWildcard = true,
                    Probability = 0.2,
                    Coefficient = 0
                }
            }
        };

        var sut = CreateSUT();

        var act = () => sut.ValidateAndThrow(slotMachine);

        Assert.Throws<ConfigurationException>(act);

    }

    [Fact]
    public void ValidateAndThrow_WhenNoSymbolsAreProvided_ThrowConfigurationException()
    {

        var slotMachine = new SlotMachine()
        {
            Name = "Test Slot Machine",
            Description = "A test machine",
            RowCount = 5,
            ColumnCount = 4,
            AnimationSpeed = 100,
            Symbols = new List<SlotMachineSymbol>()
        };

        var sut = CreateSUT();

        var act = () => sut.ValidateAndThrow(slotMachine);

        Assert.Throws<ConfigurationException>(act);

    }

    [Fact]
    public void ValidateAndThrow_WhenMachineIsValid_ReturnHealthy()
    {

        var slotMachine = new SlotMachine()
        {
            Name = "Test Slot Machine",
            Description = "A test machine",
            RowCount = 5,
            ColumnCount = 4,
            AnimationSpeed = 100,
            Symbols = new List<SlotMachineSymbol>()
            {
                new SlotMachineSymbol ()
                {
                    Name = "Test",
                    DisplayName = "Test",
                    IsWildcard = true,
                    Probability = 1,
                    Coefficient = 0
                }
            }
        };

        var sut = CreateSUT();

        sut.ValidateAndThrow(slotMachine);

    }

    [Fact]
    public async Task GenerateSymbol_WhenCalled_ReturnCorrectProbabilities()
    {

        var slotMachine = new SlotMachine()
        {
            Name = "Test Slot Machine",
            Description = "A test machine",
            RowCount = 5,
            ColumnCount = 4,
            AnimationSpeed = 100,
            Symbols = new List<SlotMachineSymbol>()
            {
                new SlotMachineSymbol ()
                {
                    Name = "Test",
                    DisplayName = "Test",
                    IsWildcard = true,
                    Probability = 0.25,
                    Coefficient = 0
                },
                new SlotMachineSymbol ()
                {
                    Name = "Test2",
                    DisplayName = "Test2",
                    IsWildcard = true,
                    Probability = 0.75,
                    Coefficient = 0
                }
            }
        };

        RNGServiceMock.Setup((x) => x.Generate()).Returns(0.5);

        var sut = CreateSUT();

        var symbol = await sut.GenerateSymbolAsync(slotMachine);

        Assert.Equal("Test2", symbol.Name);

    }

    [Fact]
    public async Task GenerateRow_WhenCalled_EnsureCorrectNumberOfSymbols()
    {

        var slotMachine = new SlotMachine()
        {
            Name = "Test Slot Machine",
            Description = "A test machine",
            RowCount = 5,
            ColumnCount = 4,
            AnimationSpeed = 100,
            Symbols = new List<SlotMachineSymbol>()
            {
                new SlotMachineSymbol ()
                {
                    Name = "Test",
                    DisplayName = "Test",
                    IsWildcard = true,
                    Probability = 0.25,
                    Coefficient = 0
                },
                new SlotMachineSymbol ()
                {
                    Name = "Test2",
                    DisplayName = "Test2",
                    IsWildcard = true,
                    Probability = 0.75,
                    Coefficient = 0
                }
            }
        };

        RNGServiceMock.Setup((x) => x.Generate()).Returns(0.5);

        var sut = CreateSUT();

        var row = await sut.GenerateRowAsync(slotMachine);

        Assert.Equal(4, row.Symbols.Count);

    }

}