using Slots.Models;
using Slots.Services;

namespace Slots.Tests;

public class UserServiceTests
{

    [Fact]
    public async Task DepositAsync_WhenValueIsBelowZero_ThrowArgumentException()
    {

        var user = new User() { UserId = Guid.NewGuid(), Forename = "John", Surname = "Lennon", Balance = 10 };

        var sut = new UserService();

        var act = async () => await sut.DepositAsync(user, -10);

        await Assert.ThrowsAsync<ArgumentException>(act);

    }

    [Fact]
    public async Task DepositAsync_WhenValueIsAboveZero_IncreaseUserBalance()
    {

        var user = new User() { UserId = Guid.NewGuid(), Forename = "John", Surname = "Lennon", Balance = 10 };

        var sut = new UserService();

        await sut.DepositAsync(user, 10);

        Assert.Equal(20, user.Balance);

    }

    [Fact]
    public async Task WithdrawAsync_WhenValueIsBelowZero_ThrowArgumentException()
    {

        var user = new User() { UserId = Guid.NewGuid(), Forename = "John", Surname = "Lennon", Balance = 10 };

        var sut = new UserService();

        var act = async () => await sut.WithdrawAsync(user, -10);

        await Assert.ThrowsAsync<ArgumentException>(act);

    }

    [Fact]
    public async Task WithdrawAsync_WhenValueIsAboveZero_DecreaseUserBalance()
    {

        var user = new User() { UserId = Guid.NewGuid(), Forename = "John", Surname = "Lennon", Balance = 25 };

        var sut = new UserService();

        await sut.WithdrawAsync(user, 10);

        Assert.Equal(15, user.Balance);

    }

}