# What is Slots

Slots is a casino platform that runs within a console application.

It is a proof of concept.

It currently has one slot-machine game called **Fruity Slots**.

But the foundations have been put in place to add more games and extend out.

# Screenshots

![Slot machine in action](Slots.Docs/Images/in-action.webm)

# Get Started

👉 Check out the [Getting Started Guide](Slots.Docs/Getting-Started-Guide.md) for a quick start

👉 Or read the [Configuration Guide](Slots.Docs/Configuration-Guide.md) to tinker with the application.

# Contribution

Want to make some pull-requests with bug fixes or new features ?

Check out the [Contribution Guide](Slots.Docs/Contribution-Guide.md) to setup a dev environment and get started

# License

Licensed under the MIT License, Copyright &copy; 2023

