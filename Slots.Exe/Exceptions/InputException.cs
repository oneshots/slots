namespace Slots.Exceptions;

/// <summary>
/// An input exception is thrown when the user has been asked for input but has provided
/// the input incorrectly or in an invalid format
/// </summary>
public sealed class InputException : Exception
{

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="message">The message to display to the user</param>
    public InputException (string message) : base (message) { }

}