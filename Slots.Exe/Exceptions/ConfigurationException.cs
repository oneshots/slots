namespace Slots.Exceptions;

/// <summary>
/// This exception is thrown when an invalid parameter has been specified in the app-settings file
/// </summary>
public sealed class ConfigurationException : Exception
{

    /// <summary>
    /// Default constructor
    /// </summary>
    public ConfigurationException(string message) : base(message) { }

}