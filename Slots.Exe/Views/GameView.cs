using Slots.Exceptions;
using Slots.Interfaces;
using Slots.Models;

namespace Slots.Screens;

/// <inheritdoc/>
public sealed class GameView : IGameView
{

    private readonly User CurrentUser;
    private readonly SlotMachine SlotMachine;
    private readonly IDisplayService DisplayService;
    private readonly IInputService InputService;
    private readonly IUserService UserService;
    private readonly ISlotMachineService SlotMachineService;
    private readonly ISlotMachineRowService SlotMachineRowService;

    public GameView(
        IDisplayService displayService,
        IUserService userService,
        IInputService inputService,
        ISlotMachineService slotMachineService,
        ISlotMachineRowService slotMachineRowService,
        SlotMachine slotMachine,
        User currentUser
    )
    {
        UserService = userService;
        InputService = inputService;
        DisplayService = displayService;
        SlotMachineService = slotMachineService;
        SlotMachineRowService = slotMachineRowService;
        CurrentUser = currentUser;
        SlotMachine = slotMachine;
    }

    public async Task RenderAsync()
    {

        DisplayService.ArtificialLoad();
        DisplayService.RenderHeading(SlotMachine.Name);
        DisplayService.RenderQuote(SlotMachine.Description);
        DisplayService.RenderText($"You have £{CurrentUser.Balance.ToString("0.00")} in your account.");
        await RenderSessionAsync();

    }

    private async Task RenderSessionAsync()
    {

        var stake = InputService.Try<decimal>(() =>
        {

            var input = InputService.GetCurrency("How much would you like to stake? (enter an amount)");

            if (input > CurrentUser.Balance)
            {

                throw new InputException("You have insufficient funds for the stake. Please try again");

            }

            return input;

        });

        await UserService.WithdrawAsync(CurrentUser, stake);

        Console.WriteLine("");

        await RenderSpinAsync(stake);

    }

    private async Task RenderSpinAsync(decimal stake)
    {

        DisplayService.RenderText("Spinning...");

        var totalWon = 0m;

        for (var rowIndex = 0; rowIndex < SlotMachine.RowCount; rowIndex++)
        {

            var row = await SlotMachineService.GenerateRowAsync(SlotMachine);

            var rowValue = SlotMachineRowService.CalculateValue(row, stake);

            foreach (var symbol in row.Symbols)
            {

                foreach (var fakeSymbol in SlotMachine.Symbols)
                {

                    Console.Write(fakeSymbol.DisplayName);

                    await Task.Delay(SlotMachine.AnimationSpeed);

                    DisplayService.RemoveLastCharacter();

                }

                Console.Write(symbol.DisplayName + " ");

                await Task.Delay(SlotMachine.AnimationSpeed);

            }

            Console.Write($" | £{rowValue.ToString("0.00")}");

            Console.WriteLine("");

            Console.WriteLine("");

            totalWon += rowValue;

        }

        await UserService.DepositAsync(CurrentUser, totalWon);

        DisplayService.RenderText($"You won £{totalWon.ToString("0.00")}");

        DisplayService.RenderText($"You now have £{CurrentUser.Balance.ToString("0.00")} in your account.");

        await InputService.PromptAsync(
            new PromptOption("Spin again", async () => await RenderSessionAsync()),
            new PromptOption("Return home")
        );

    }

}