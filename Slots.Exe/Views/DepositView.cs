using Slots.Interfaces;
using Slots.Models;

namespace Slots.Screens;

/// <inheritdoc/>
public sealed class DepositView : IDepositView
{

    private readonly User CurrentUser;
    private readonly IDisplayService DisplayService;
    private readonly IInputService InputService;
    private readonly IUserService UserService;

    public DepositView
    (
        User currentUser,
        IDisplayService displayService,
        IUserService userService,
        IInputService inputService
    )
    {
        CurrentUser = currentUser;
        DisplayService = displayService;
        InputService = inputService;
        UserService = userService;
    }

    public async Task RenderAsync()
    {

        DisplayService.ArtificialLoad();
        DisplayService.RenderHeading("💸 Despoit Funds");
        var value = InputService.GetCurrency("How much would you like to deposit into your account?");
        Console.WriteLine("");
        DisplayService.RenderText("Sending funds...");
        await UserService.DepositAsync(CurrentUser, value);
        DisplayService.RenderText("Funds have been successfully added to your account");
        await InputService.PromptAsync(
            new PromptOption("Make another deposit", async () => await RenderAsync()),
            new PromptOption("Return home")
        );

    }

}