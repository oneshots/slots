using Slots.Interfaces;
using Slots.Models;

namespace Slots.Screens;

/// <inheritdoc/>
public sealed class AccountInformationView : IAccountInformationView
{

    private readonly User CurrentUser;
    private readonly IDisplayService DisplayService;
    private readonly IInputService InputService;

    public AccountInformationView
    (
        User currentUser,
        IDisplayService displayService,
        IInputService inputService
    )
    {
        CurrentUser = currentUser;
        DisplayService = displayService;
        InputService = inputService;
    }

    public async Task RenderAsync()
    {

        await Task.Run(async () =>
        {

            DisplayService.ArtificialLoad();
            DisplayService.RenderHeading("😊 Account Information");
            Console.WriteLine("Below are the details we have stored in your account:");
            Console.WriteLine("");
            Console.WriteLine($"ID: {CurrentUser.UserId}");
            Console.WriteLine($"Forename: {CurrentUser.Forename}");
            Console.WriteLine($"Surname: {CurrentUser.Surname}");
            Console.WriteLine($"Balance: {CurrentUser.Balance.ToString("0.00")}");
            Console.WriteLine("");
            await InputService.PromptAsync(
                new PromptOption("Return home")
            );

        });

    }

}