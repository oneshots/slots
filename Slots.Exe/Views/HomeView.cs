using Slots.Interfaces;
using Slots.Models;

namespace Slots.Screens;

/// <inheritdoc/>
public sealed class HomeView : IHomeView
{

    private readonly User CurrentUser;
    private readonly IDisplayService DisplayService;
    private readonly IInputService InputService;
    private readonly IAccountInformationView AccountInformationView;
    private readonly IDepositView DepositView;
    private readonly IGameView GameView;

    public HomeView(
        User currentUser,
        SlotMachine slotMachine,
        IAccountInformationView accountInformationScreen,
        IDepositView depositScreen,
        IInputService inputService,
        IGameView gameScreen,
        IDisplayService displayService
    )
    {
        CurrentUser = currentUser;
        AccountInformationView = accountInformationScreen;
        GameView = gameScreen;
        DepositView = depositScreen;
        DisplayService = displayService;
        InputService = inputService;
    }

    public async Task RenderAsync()
    {

        DisplayService.ArtificialLoad();

        DisplayService.RenderHeading("🏠 Home Screen");

        DisplayService.RenderText($"Welcome back, {CurrentUser.Forename}\nYou have £{CurrentUser.Balance.ToString("0.00")} in your account. 💰");

        var options = new List<PromptOption>()
        {
            new PromptOption("Deposit funds", async () => await DepositView.RenderAsync()),
            new PromptOption("View account information", async () => await AccountInformationView.RenderAsync()),
            new PromptOption("Exit", async () => await Task.Run(() => Environment.Exit(0)))
        };

        if (CurrentUser.Balance > 0)
        {

            options.Insert(0, new PromptOption("Play the slot machine 🕹️", async () => await GameView.RenderAsync()));
        }

        await InputService.PromptAsync(options.ToArray());

        await RenderAsync();

    }

}