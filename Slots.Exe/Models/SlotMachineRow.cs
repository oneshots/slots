namespace Slots.Models;

/// <summary>
/// Represents a single row that was spun in the slot machine.
/// Contains the symbols that make up the row and the win state of the row.
/// </summary>
public sealed class SlotMachineRow
{

    /// <summary>
    /// The symbols in this row
    /// </summary>
    public required List<SlotMachineSymbol> Symbols { get; init; }

}