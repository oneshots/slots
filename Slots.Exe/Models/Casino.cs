namespace Slots.Models;

/// <summary>
/// Represents the casino that runs this application
/// </summary>
public sealed class Casino
{

    /// <summary>
    /// Name of the casino.
    /// This is what will appear on all screen headings.
    /// </summary>
    public required string Name { get; init; }

}