namespace Slots.Models;

/// <summary>
/// Represents a symbol that can be displayed by the slot-machine
/// </summary>
public sealed class SlotMachineSymbol
{

    /// <summary>
    /// Name of the symbol
    /// </summary>
    public required string Name { get; init; }

    /// <summary>
    /// The name of the symbol as it appears in the game
    /// </summary>
    public required string DisplayName { get; init; }

    /// <summary>
    /// The coefficient of the symbol that is used in the "win amount" calculation
    /// </summary>
    public required decimal Coefficient { get; init; }

    /// <summary>
    /// The probability that this symbol will appear in a slot-machine cell on each spin
    /// </summary>
    public required double Probability { get; init; }

    /// <summary>
    /// Determines if this symbol acts as a wildcard (can be matched with any other symbols)
    /// </summary>
    public required bool IsWildcard { get; init; }

}