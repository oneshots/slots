namespace Slots.Models;

/// <summary>
/// Represents an option that is presented to the user during a display prompt
/// </summary>
public sealed class PromptOption
{

    /// <summary>
    /// Name of the prompt
    /// </summary>
    public string Name { get; }

    /// <summary>
    /// The action to be executed if the prompt is selected
    /// </summary>
    public Func<Task>? Action { get; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public PromptOption(string name, Func<Task>? action = null)
    {

        Name = name;

        Action = action;

    }

}