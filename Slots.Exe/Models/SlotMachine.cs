namespace Slots.Models;

/// <summary>
/// Represents a digital slot machine
/// </summary>
public sealed class SlotMachine
{

    /// <summary>
    /// Name of the slot machine
    /// </summary>
    public required string Name { get; init; }

    /// <summary>
    /// A brief description of the slot machine
    /// </summary>
    public required string Description { get; init; }

    /// <summary>
    /// The number of milliseconds between frames of the symbol spin animation
    /// </summary>
    public required int AnimationSpeed { get; init; }

    /// <summary>
    /// The number of rows in the slot machine
    /// </summary>
    public required int RowCount { get; init; }

    /// <summary>
    /// The number of columns / symbols in each row of the slot machine
    /// </summary>
    public required int ColumnCount { get; init; }

    /// <summary>
    /// List of possible symbols that can be output by this slot machine
    /// </summary>
    public required List<SlotMachineSymbol> Symbols { get; init; }
}