using Slots.Interfaces;

namespace Slots.Models;

/// <summary>
/// The user model holds information about a user in the system and is intended
/// to be persisted in a database / backing store.
/// </summary>
public sealed class User
{

    /// <summary>
    /// Unique identifier of the user.
    /// </summary>
    public required Guid UserId { get; init; }

    /// <summary>
    /// Forename of the user. This will be used by the game to show personalized messages to the user.
    /// </summary>
    public required string Forename { get; init; }

    /// <summary>
    /// Surname of the user. This isn't used as commonly as forename but is kept to mimic a real system.
    /// </summary>
    public required string Surname { get; init; }

    /// <summary>
    /// The user's current balance.
    /// </summary>
    public required decimal Balance { get; set; }

}