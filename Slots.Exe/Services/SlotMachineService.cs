using Slots.Exceptions;
using Slots.Interfaces;
using Slots.Models;

namespace Slots.Services;

/// <inheritdoc/>
public sealed class SlotMachineService : ISlotMachineService
{

    /// <summary>
    /// Random Number Generation Service
    /// </summary>
    private readonly IRNGService RNGService;

    /// <summary>
    /// Default constructor
    /// </summary>
    public SlotMachineService(IRNGService rngService)
    {

        RNGService = rngService;

    }

    /// <inheritdoc/>
    public async Task<SlotMachineRow> GenerateRowAsync(SlotMachine slotMachine)
    {

        var symbols = new List<SlotMachineSymbol>();

        for (var columnIndex = 0; columnIndex < slotMachine.ColumnCount; columnIndex++)
        {

            var symbol = await GenerateSymbolAsync(slotMachine);

            symbols.Add(symbol);

        }

        var row = new SlotMachineRow()
        {
            Symbols = symbols
        };

        return row;

    }

    /// <inheritdoc/>
    public async Task<SlotMachineSymbol> GenerateSymbolAsync(SlotMachine slotMachine)
    {

        var possibleSymbols = slotMachine.Symbols.OrderByDescending(x => x.Probability);

        var rolled = RNGService.Generate();

        foreach (var symbol in possibleSymbols)
        {

            if (rolled < symbol.Probability)
            {

                return await Task.FromResult(symbol);

            }

            rolled -= symbol.Probability;

        }

        throw new Exception("Probability mismatch");

    }

    /// <inheritdoc/>
    public void ValidateAndThrow(SlotMachine slotMachine)
    {

        if (slotMachine.Symbols.Count == 0)
        {

            throw new ConfigurationException("Slot machine must have one or more symbols to operate");

        }

        var totalProbability = slotMachine.Symbols.Sum((x) => x.Probability);

        if (totalProbability != 1)
        {

            throw new ConfigurationException("The defined symbols in the slot machine must have a total probability of 1.0");

        }

        if (slotMachine.ColumnCount <= 0)
        {

            throw new ConfigurationException("Slot machine must have at least one column");

        }

        if (slotMachine.RowCount <= 0)
        {

            throw new ConfigurationException("Slot machine must have at least one row");

        }

        if (slotMachine.AnimationSpeed <= 0)
        {

            throw new ConfigurationException("Slot machine animation speed must be a positive integer");

        }

    }

}