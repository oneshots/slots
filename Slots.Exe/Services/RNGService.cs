using Slots.Interfaces;

namespace Slots.Services;

/// <inheritdoc/>
public sealed class RNGService : IRNGService
{

    /// <summary>
    /// Random Number Generator
    /// </summary>
    private readonly Random Random;

    /// <summary>
    /// Default constructor
    /// </summary>
    public RNGService()
    {

        Random = new Random();

    }

    /// <inheritdoc/>
    public double Generate()
    {

        var randomNumber = Random.NextDouble();

        return randomNumber;

    }

}