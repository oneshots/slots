using Slots.Interfaces;
using Slots.Models;

namespace Slots.Services;

/// <inheritdoc/>
public sealed class SlotMachineRowService : ISlotMachineRowService
{

    /// <inheritdoc/>
    public decimal CalculateValue(SlotMachineRow slotMachineRow, decimal stake)
    {

        var symbolsWithoutWildcards = slotMachineRow.Symbols.Where(x => x.IsWildcard == false).ToList();

        var symbolsMatch = symbolsWithoutWildcards.All(x => x.Name == symbolsWithoutWildcards[0].Name);

        if (symbolsMatch)
        {

            var value = slotMachineRow.Symbols.Sum(x => x.Coefficient) * stake;

            return value;

        }

        return 0;
    
    }

}