using Slots.Interfaces;
using Slots.Models;

namespace Slots.Services;

/// <inheritdoc/>
public sealed class UserService : IUserService
{

    /// <inheritdoc/>
    public async Task DepositAsync(User user, decimal value)
    {

        if (value < 0)
        {

            throw new ArgumentException("Value cannot be less than zero. Please try again");

        }

        user.Balance += value;

        await Task.Delay(750); // artificial delay

        // NOTE: This would usually be persisted to database

    }

    /// <inheritdoc/>
    public async Task WithdrawAsync(User user, decimal value)
    {

        if (value < 0)
        {

            throw new ArgumentException("Value must be greater than zero. Please try again");

        }

        user.Balance -= value;

        await Task.Delay(750); // artificial delay

        // NOTE: This would usually be persisted to database

    }

}