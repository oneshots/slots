using Slots.Exceptions;
using Slots.Interfaces;
using Slots.Models;

namespace Slots.Services;

/// <inheritdoc/>
public sealed class InputService : IInputService
{

    /// <inheritdoc/>
    public T Try<T>(Func<T> delegateMethod)
    {

        while (true)
        {

            try
            {

                var result = delegateMethod();

                return result;

            }

            catch (Exception e)
            {

                Console.WriteLine(e.Message);

            }

        }

    }

    /// <inheritdoc/>
    public decimal GetCurrency(string? question = null)
    {

        if (question != null)
        {

            Console.WriteLine(question);

        }

        return Try<decimal>(() =>
        {

            Console.Write("> £");

            var input = Console.ReadLine();

            var inputIsNullOrEmpty = string.IsNullOrEmpty(input);

            if (inputIsNullOrEmpty)
            {

                throw new InputException("No value was entered. Please try again");

            }

            var inputIsDecimal = decimal.TryParse(input, out decimal inputDecimal);

            if (inputIsDecimal == false)
            {

                throw new InputException("The value entered is not a decimal. Please try again");

            }

            var inputHasDecimals = input!.Split(".").Count() > 1;

            if (inputHasDecimals)
            {

                var inputNumberOfDecimals = input.Split(".").Last().Length;

                if (inputNumberOfDecimals > 2)
                {

                    throw new InputException("Value should only have two decimal places. Please try again");

                }

            }

            if (inputDecimal < 0)
            {

                throw new InputException("Value should be greater than zero. Please try again");

            }

            return inputDecimal;

        });

    }

    /// <inheritdoc/>
    public async Task PromptAsync(params PromptOption[] options)
    {

        Console.WriteLine("Options:");

        for (var i = 0; i < options.Count(); i++)
        {

            var option = options[i];

            var optionIndex = i + 1;

            Console.WriteLine($"({optionIndex}) {option.Name}");

        }

        var optionId = Try<int>(() =>
        {

            Console.WriteLine("----");

            Console.Write("> Choice: ");

            var input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {

                throw new InputException("No choice was provided. Please try again");

            }

            var inputIsNumeric = int.TryParse(input, out var inputInt);

            if (inputIsNumeric == false)
            {

                throw new InputException("The choice you have entered is not a number. Please try again");

            }

            if (inputInt <= 0 || inputInt > options.Count())
            {

                throw new InputException("The choice you have selected does not exist. Please try again");

            }

            return inputInt;

        });

        var selectedOption = options.ElementAt(optionId - 1);

        if (selectedOption.Action != null)
        {

            await selectedOption.Action();

        }

    }
}