using Slots.Interfaces;
using Slots.Models;

namespace Slots.Services;

/// <inheritdoc/>
public sealed class DisplayService : IDisplayService
{

    private readonly Casino Casino;

    /// <summary>
    /// Default constructor
    /// </summary>
    public DisplayService(Casino casino)
    {

        Casino = casino;

    }

    /// <inheritdoc/>
    public void ArtificialLoad()
    {

        Console.WriteLine("");

        Console.WriteLine("Loading...");

        Thread.Sleep(750);

        Console.WriteLine("");

    }

    /// <inheritdoc/>
    public void RenderHeading(string title)
    {

        Console.WriteLine("================================");
        Console.WriteLine(Casino.Name);
        Console.WriteLine("================================");
        Console.WriteLine(title);
        Console.WriteLine("--------------------------------");
        Console.WriteLine("");

    }

    /// <inheritdoc/>
    public void RemoveLastCharacter()
    {

        Console.Write("\x1B[1D");
        Console.Write("\x1B[1P");
        Console.Write("\x1B[1D");
        Console.Write("\x1B[1P");

    }

    /// <inheritdoc/>
    public void RenderText(string text)
    {

        Console.WriteLine(text);
        Console.WriteLine("");

    }

    /// <inheritdoc/>
    public void RenderQuote(string quote)
    {

        Console.WriteLine("~~~");
        Console.WriteLine(quote);
        Console.WriteLine("~~~");
        Console.WriteLine("");

    }

}