using Slots.Models;

namespace Slots.Interfaces;

/// <summary>
/// The input service allows the program to receive specific kinds of input from the user.
/// </summary>
public interface IInputService
{

    /// <summary>
    /// Prompts the user with a list of options they can choose from
    /// </summary>
    Task PromptAsync(params PromptOption[] options);

    /// <summary>
    /// Prompts the user to enter a monetary value into the console (£XX.XX)
    /// </summary>
    /// <param name="question">Optionally, a question to be asked before prompting input</param>
    decimal GetCurrency(string? question = null);

    /// <summary>
    /// Runs the provided delegate.
    /// If an InputException is thrown by the delegate then the delegate will be re-run.
    /// </summary>
    /// <param name="delegateMethod">The delegate to execute (usually with a question)</param>
    /// <typeparam name="T">The type to return from the delegate</typeparam>
    T Try<T>(Func<T> delegateMethod);

}