using Slots.Models;

namespace Slots.Interfaces;

/// <summary>
/// The user service is responsible for fetching and manipulating user data models
/// </summary>
public interface IUserService
{
    /// <summary>
    /// Withdraw money from a user's account
    /// </summary>
    /// <param name="user">The user to withdraw money from</param>
    /// <param name="value">The value to withdraw</param>
    Task WithdrawAsync(User user, decimal value);

    /// <summary>
    /// Deposit money into a user's account
    /// </summary>
    /// <param name="user">The user to deposit money to</param>
    /// <param name="value">The value to deposit</param>
    Task DepositAsync(User user, decimal value);

}