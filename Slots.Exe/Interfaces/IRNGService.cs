namespace Slots.Interfaces;

/// <summary>
/// Random Number Generation Service. This is used for unit-testing purposes
/// </summary>
public interface IRNGService
{

    /// <summary>
    /// Generates a double floating-pointer number between 0 and 1
    /// </summary>
    double Generate();

}