namespace Slots.Interfaces;

/// <summary>
/// The deposit screen asks the user how much they would like to deposit into their account and
/// then performs the action.
/// </summary>
public interface IDepositView : IView { }