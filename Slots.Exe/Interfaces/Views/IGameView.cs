namespace Slots.Interfaces;

/// <summary>
/// The game screen is the entry-point for the slot machine game. It controls
/// the state of the slot machine, takes input from the user and renders the output.
/// </summary>
public interface IGameView : IView { }