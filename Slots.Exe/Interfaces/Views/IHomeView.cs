namespace Slots.Interfaces;

/// <summary>
/// The home view is used to welcome the user to the application and provide them with
/// a number of different options they can do in the program.
/// </summary>
public interface IHomeView : IView { }