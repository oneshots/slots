namespace Slots.Interfaces;

/// <summary>
/// A view is a sub-section of the console application which takes input and renders output to the user.
/// </summary>
public interface IView
{

    /// <summary>
    /// Renders a view to the console
    /// </summary>
    Task RenderAsync();

}