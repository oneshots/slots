namespace Slots.Interfaces;

/// <summary>
/// The account information view shows a break-down of the current user's details
/// </summary>
public interface IAccountInformationView : IView { }