namespace Slots.Interfaces;

/// <summary>
/// The display service is an abstraction over the default .NET console with some methods that make
/// development of games more concise and to DRY the codebase
/// </summary>
public interface IDisplayService
{

    /// <summary>
    /// Creates an artificial load screen.
    /// This is done to prevent the user from getting whiplash when moving between screens.
    /// </summary>
    void ArtificialLoad();

    /// <summary>
    /// Renders a heading with a title for the user to read
    /// </summary>
    void RenderHeading(string title);

    /// <summary>
    /// Renders a quote for the user to read
    /// </summary>
    void RenderQuote(string quote);

    /// <summary>
    /// Removes the last character that was output
    /// </summary>
    void RemoveLastCharacter();

    /// <summary>
    /// Renders a block of text for the user to read
    /// </summary>
    /// <param name="text">The text to display</param>
    void RenderText(string text);

}