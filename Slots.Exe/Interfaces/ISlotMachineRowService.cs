using Slots.Models;

namespace Slots.Interfaces;

/// <summary>
/// This service provides functionality to slot machine rows
/// </summary>
public interface ISlotMachineRowService
{

    /// <summary>
    /// Calculates the monetary value of a slot machine row based on the coefficient
    /// of the symbols in the row multiplied by the user's stake.
    /// </summary>
    /// <param name="slotMachineRow">The slot machine row to perform calculation on</param>
    /// <param name="stake">The stake provided by the user</param>
    /// <returns>A decimal value representing the total monetary value of the row</returns>
    decimal CalculateValue (SlotMachineRow slotMachineRow, decimal stake);

}