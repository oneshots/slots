using Slots.Models;

namespace Slots.Interfaces;

/// <summary>
/// This service is responsible for managing the state of slot machines
/// </summary>
public interface ISlotMachineService
{

    /// <summary>
    /// Randomly generates a new symbol from a slot machine
    /// </summary>
    /// <param name="slotMachine">The slot machine to use</param>
    /// <returns>A randomly generated symbol based on the possible symbol list stored in the machine</returns>
    Task<SlotMachineSymbol> GenerateSymbolAsync(SlotMachine slotMachine);

    /// <summary>
    /// Randomly generates a row of symbols from a slot machine
    /// </summary>
    /// <param name="slotMachine">The slot machine to use</param>
    /// <returns>A randomly generated row of symbols based on the possible symbol list stored in the machine</returns>
    Task<SlotMachineRow> GenerateRowAsync(SlotMachine slotMachine);

    /// <summary>
    /// Validates the configuration of a slot machine and throws an exception if there are any problems
    /// </summary>
    void ValidateAndThrow(SlotMachine slotMachine);

}