using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Slots.Interfaces;
using Slots.Models;

namespace Slots.Extensions;

/// <summary>
/// Provides additional methods to the {IServiceCollection}.
/// This is mostly used to keep the program.cs file clean.
/// </summary>
public static class IServiceCollectionExtensions
{

    /// <summary>
    /// Adds the currently authenticated user as a scoped object
    /// </summary>
    public static void AddCurrentUser(this IServiceCollection serviceCollection)
    {

        serviceCollection.AddScoped<User>((services) =>
        {

            // NOTE: In a real world scenario, this would be determined via a web token (or similar)
            // but for now this is mock data

            return new User()
            {
                UserId = Guid.NewGuid(),
                Forename = "Greg",
                Surname = "Kennedy",
                Balance = 0m
            };

        });

    }

    /// <summary>
    /// Adds the slot machine object as a scoped instance
    /// </summary>
    public static void AddSlotMachine(this IServiceCollection serviceCollection)
    {

        serviceCollection.AddScoped<SlotMachine>((services) =>
        {
            var slotMachineService = services.GetRequiredService<ISlotMachineService>();
            var configuration = services.GetRequiredService<IConfiguration>();
            var slotMachine = configuration.GetRequiredSection("SlotMachine").Get<SlotMachine>();
            if (slotMachine == null)
            {
                throw new Exception("Could not create slot machine from the appsettings config file");
            }
            slotMachineService.ValidateAndThrow(slotMachine);
            return slotMachine;
        });

    }

    /// <summary>
    /// Adds the casino (owner of the application) as a scoped object
    /// </summary>
    public static void AddCasino(this IServiceCollection serviceCollection)
    {

        serviceCollection.AddScoped<Casino>((services) =>
        {
            var configuration = services.GetRequiredService<IConfiguration>();
            var casino = configuration.GetRequiredSection("Casino").Get<Casino>();
            if (casino == null)
            {
                throw new Exception("Could not create casino from the appsettings config file");
            }
            return casino;
        });

    }

}