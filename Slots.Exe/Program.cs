﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Slots.Extensions;
using Slots.Interfaces;
using Slots.Screens;
using Slots.Services;

using IHost host = Host.CreateDefaultBuilder(args)
.ConfigureServices(services =>
{
    services.AddScoped<IUserService, UserService>();
    services.AddScoped<IAccountInformationView, AccountInformationView>();
    services.AddScoped<IHomeView, HomeView>();
    services.AddScoped<IDepositView, DepositView>();
    services.AddScoped<IGameView, GameView>();
    services.AddScoped<IDisplayService, DisplayService>();
    services.AddScoped<IInputService, InputService>();
    services.AddScoped<ISlotMachineService, SlotMachineService>();
    services.AddScoped<ISlotMachineRowService, SlotMachineRowService>();
    services.AddScoped<IRNGService, RNGService>();
    services.AddCurrentUser();
    services.AddSlotMachine();
    services.AddCasino();
})
.Build();

var homeView = host.Services.GetService<IHomeView>();

if (homeView != null)
{

    await homeView.RenderAsync();

}