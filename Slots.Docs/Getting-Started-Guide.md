# Getting Started Guide

## Requirements

In order to run Slots you will need the following to be installed on your system:

- **Docker**

    Slots is a containerized application. 
    
    Docker will allow you to run the application without a dev environment.

- **Windows Terminal**

    ⚠️ Slots uses emojis for UI and will **not** work on the older windows command-prompt or older versions of Powershell.

## Running via Docker

Slots has been containerized for ease of access.

The container is hosted in Gitlab with the codebase.

With docker installed, you can execute the following command in Windows Terminal:

```
docker run -it registry.gitlab.com/oneshots/slots:latest
```

Alternatively, you can run the program from the codebase (more details: [Contribution Guide](Contribution-Guide.md))

## Screens

When slots has started, you will have access to a number of screens

A breakdown of these screens can be found below:

### Home Screen

![alt text](Images/home-screen.png)

The first screen you will see is the **Home Screen**

This will welcome the user and show them their account balance

The user will then have the option to:

1. Play the slot machine game

2. Deposit more funds into their account

3. View their account details

4. Exit the application

The user can input a number to select an option

Each option will link to a new screen

### Game Screen

![alt text](Images/game-screen.png)

The game screen is where the slot machine can be operated

The slot machine will ask you to enter a stake amount

This will be how much you want to gamble

Once a value has been entered, the money will be withdrawn from your account

The slot machine (by default) has 4 rows and 3 columns

Each column will spin for a few seconds before revealing the outcome

Once all rows have been revealed, the amount won will be displayed

Your new account balance will then be shown

### Account Information Screen

![alt text](Images/account-information-screen.png)

The account information screen is a breakdown of your account

It shows what details about you we have stored on our system

This is currently very minimal but can be expanded upon

### Deposit Screen

![alt text](Images/deposit-screen.png)

The deposit screen can be used to input money to your acount

This is useful if your funds are getting low