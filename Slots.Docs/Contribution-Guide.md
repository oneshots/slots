# Contribution Guide

## Creating a development environment

### Dependencies

Before getting started, please note the following dependencies for the dev environment:

- Docker Desktop
- VSCode
- VSCode Remote Container Extension

### Guide

Follow the below steps to create a development environment for Slots:

1. Clone the repository to a location of your choice

2. Open the repository in **VSCode**

3. When prompted, click **Reopen in Container**

This will spin up a new containerized dev environment for the application to be developed in

---

## Coding Conventions

- **Use a postfix on asynchronous methods**

    Asynchronous methods names must be post-fixed with "Async". 

    E.g `GetCurrentUserAsync()`.

    This will make async methods identifiable at a glance.

- **Sealed classes where possible**

    Classes should be marked as `sealed` where appropriate.

    This is to enhance compile and run-time performance.

- **Services must have interfaces**

    All services are required to have interfaces.

    This is to reduce dependency on concrete implementations.

---

## Commit Convention

This is a monorepo.

As such, it's important to make commit messages as clear as possible.

Below is a standard commit message template:

`<type>: <description>`

Examples of this template in use:

- `feat: added withdraw method to the user service`
- `docs: fixed spelling error in contribution guide`
- `bug: fixed issue with slot machine spinning out of control`

---

## Pull Requests

All fixes, features, etc. should be merged to the main branch via a pull request

The pull request will remain open for a couple of days to give everyone a chance to review

Once build pipelines have been setup, all pull-requests should run through automated unit testing