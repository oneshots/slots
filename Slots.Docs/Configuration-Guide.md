# Configuration Guide

I have tried to make this application as modular and configurable as possible.

As such, a lot of config values are read from the `appsettings.json` file at run-time.

Below is an image of the app settings file and a breakdown of each option.

![alt text](Images/app-settings.png)

`Casino` - This section contains information about the casino that deploys the application.

`Casino.Name` - This is the company name of the casino

`SlotMachine` - This section contains information about our 'Fruity Slots' slot machine

`SlotMachine.Name` - The name of the slot machine as it appears in game

`SlotMachine.Description` - A description of the slot machine as it appears in game

`SlotMachine.RowCount` - The number of rows the slot machine will display when spun

`SlotMachine.ColumnCount` - The number of symbols each row in the slot machine will have

`SlotMachine.AnimationSpeed` - The speed of the slot machine animations (in milliseconds)

`SlotMachine.Symbols` - A list of symbols that can be displayed by the slot machine

`SlotMachine.Symbols.Name` - The internal name of a symbol

`SlotMachine.Symbols.DisplayName` - The name of a symbol as it appears in game

`SlotMachine.Symbols.Coefficient` - The coefficient of a symbol for use in winning calc

`SlotMachine.Symbols.Probability` - The probability that a symbol will appear

`SlotMachine.Symbols.IsWildcard` - Determines if the symbol acts as a wildcard

All of these options can be modified so feel free to play around